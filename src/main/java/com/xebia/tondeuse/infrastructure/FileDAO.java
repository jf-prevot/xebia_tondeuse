package com.xebia.tondeuse.infrastructure;

import com.xebia.tondeuse.Main;
import com.xebia.tondeuse.domain.model.COMMAND;
import com.xebia.tondeuse.domain.model.FieldDimensions;
import com.xebia.tondeuse.domain.model.InstructionsFile;
import com.xebia.tondeuse.domain.model.MowerInstruction;
import com.xebia.tondeuse.domain.model.ORIENTATION;
import com.xebia.tondeuse.dto.Position;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileDAO {

  private static final String SPACE = " ";

  public InstructionsFile read(final String filePath) throws IOException {
    final List<String> lines;
    final InputStream resourceAsStream = Main.class.getResourceAsStream("/" + filePath);
    if (resourceAsStream == null) {
      throw new FileNotFoundException();
    }
    try (BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream))) {
      lines = br.lines().collect(Collectors.toList());
    }
    return parseFileContent(lines);
  }

  private InstructionsFile parseFileContent(final List<String> lines) {
    try {
      final FieldDimensions fieldDimensions = parseFieldDimensions(lines);
      final List<MowerInstruction> mowerInstructions = parseMowerInstructions(lines);
      return new InstructionsFile(fieldDimensions, mowerInstructions);
    } catch (Exception ex) {
      throw new IllegalArgumentException("File format error");
    }
  }

  private List<MowerInstruction> parseMowerInstructions(final List<String> lines) {
    final List<MowerInstruction> mowerInstructions = new ArrayList<>();
    for (int i = 0; i < lines.size(); i += 2) {
      final MowerInstruction mowerInstruction = parseMowerInstruction(lines, i);
      mowerInstructions.add(mowerInstruction);
    }
    return mowerInstructions;
  }

  private MowerInstruction parseMowerInstruction(final List<String> lines, final int i) {
    final String[] initialPosition = lines.get(i).split(SPACE);
    final Integer x = Integer.parseInt(initialPosition[0]);
    final Integer y = Integer.parseInt(initialPosition[1]);
    final ORIENTATION orientation = ORIENTATION.valueOf(initialPosition[2]);
    final List<COMMAND> commands = lines.get(i + 1)
                                        .chars().mapToObj(c -> String.valueOf((char) c))
                                        .map(COMMAND::valueOf).collect(Collectors.toList());
    return new MowerInstruction(new Position(x, y, orientation), commands);
  }

  private FieldDimensions parseFieldDimensions(final List<String> lines) {
    final String[] rawLawnSize = lines.get(0).split(SPACE);
    final Integer width = Integer.parseInt(rawLawnSize[0]);
    final Integer height = Integer.parseInt(rawLawnSize[1]);
    lines.remove(0);
    return new FieldDimensions(width, height);
  }
}
