package com.xebia.tondeuse.dto;

import com.xebia.tondeuse.domain.model.ORIENTATION;

public class Position {

  private Integer x;
  private Integer y;
  private ORIENTATION orientation;

  public Position(final Integer x, final Integer y, final ORIENTATION orientation) {
    this.x = x;
    this.y = y;
    this.orientation = orientation;
  }

  public Integer getX() {
    return x;
  }

  public Integer getY() {
    return y;
  }

  public ORIENTATION getOrientation() {
    return orientation;
  }

  public void setX(final Integer x) {
    this.x = x;
  }

  public void setY(final Integer y) {
    this.y = y;
  }

  public void setOrientation(final ORIENTATION orientation) {
    this.orientation = orientation;
  }

  @Override
  public String toString() {
    return x + " " + y + " " + orientation;
  }
}
