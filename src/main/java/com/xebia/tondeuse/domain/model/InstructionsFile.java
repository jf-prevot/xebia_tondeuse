package com.xebia.tondeuse.domain.model;

import java.util.List;

public class InstructionsFile {

  private FieldDimensions fieldDimensions;
  private List<MowerInstruction> commands;

  public InstructionsFile(final FieldDimensions fieldDimensions, final List<MowerInstruction> commands) {
    this.fieldDimensions = fieldDimensions;
    this.commands = commands;
  }

  public FieldDimensions getFieldDimensions() {
    return fieldDimensions;
  }

  public List<MowerInstruction> getCommands() {
    return commands;
  }
}
