package com.xebia.tondeuse.domain.model;

public enum ORIENTATION {
  N, E, W, S
}
