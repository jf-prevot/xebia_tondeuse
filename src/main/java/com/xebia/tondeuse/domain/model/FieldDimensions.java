package com.xebia.tondeuse.domain.model;

public class FieldDimensions {

  private final Integer width;
  private final Integer height;

  public FieldDimensions(final Integer width, final Integer height) {
    this.width = width;
    this.height = height;
  }

  Integer getWidth() {
    return width;
  }

  Integer getHeight() {
    return height;
  }
}
