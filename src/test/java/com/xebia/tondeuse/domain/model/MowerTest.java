package com.xebia.tondeuse.domain.model;

import com.xebia.tondeuse.dto.Position;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static com.xebia.tondeuse.domain.model.ORIENTATION.*;
import static org.assertj.core.api.Assertions.assertThat;

class MowerTest {

  private Mower mower;
  private static final FieldDimensions FIELD_DIMENSIONS = new FieldDimensions(2, 2);

  @Test
  void turnLeft() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, E));

    final Position position = mower.execute(Collections.singletonList(COMMAND.G));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 0, N));
  }

  @Test
  void turnRight() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, N));

    final Position position = mower.execute(Collections.singletonList(COMMAND.D));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 0, E));
  }

  @Test
  void turn_around_right() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, N));

    final Position position = mower.execute(Arrays.asList(COMMAND.D, COMMAND.D, COMMAND.D, COMMAND.D));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 0, N));
  }

  @Test
  void turn_around_left() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, S));

    final Position position = mower.execute(Arrays.asList(COMMAND.G, COMMAND.G, COMMAND.G, COMMAND.G));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 0, S));
  }

  @Test
  void turnLeft_and_goNorth() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, E));

    final Position position = mower.execute(Arrays.asList(COMMAND.G, COMMAND.A));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 1, N));
  }

  @Test
  void turnRight_and_goEast() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, N));

    final Position position = mower.execute(Arrays.asList(COMMAND.D, COMMAND.A));

    assertThat(position).isEqualToComparingFieldByField(new Position(1, 0, E));
  }

  @Test
  void large_turn_around() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, N));

    final Position position = mower.execute(Arrays.asList(COMMAND.A, COMMAND.D, COMMAND.A, COMMAND.D, COMMAND.A, COMMAND.D, COMMAND.A, COMMAND.D));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 0, N));
  }

  @Test
  void forward_until_North_limit() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, N));

    final Position position = mower.execute(Arrays.asList(COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.A));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 2, N));
  }

  @Test
  void forward_until_East_limit() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 0, E));

    final Position position = mower.execute(Arrays.asList(COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.A));

    assertThat(position).isEqualToComparingFieldByField(new Position(2, 0, E));
  }

  @Test
  void forward_until_South_limit() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(0, 1, S));

    final Position position = mower.execute(Arrays.asList(COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.A));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 0, S));
  }

  @Test
  void forward_until_West_limit() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(1, 0, W));

    final Position position = mower.execute(Arrays.asList(COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.A));

    assertThat(position).isEqualToComparingFieldByField(new Position(0, 0, W));
  }

  @Test
  void very_large_turn_around() {
    mower = new Mower(FIELD_DIMENSIONS, new Position(2, 0, N));

    final Position position = mower.execute(Arrays.asList(COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.G,
                                                          COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.G,
                                                          COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.G,
                                                          COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.G, COMMAND.G));

    assertThat(position).isEqualToComparingFieldByField(new Position(2, 0, W));
  }
}